import os
import random

from aiogram import types

from config import bot


# @dp.message_handler(commands=["start"])
async def start(message: types.Message):
    """Привествует пользователя"""
    print(f"{message.from_user=}")

    kb = types.InlineKeyboardMarkup()
    kb.add(
        types.InlineKeyboardButton("Мы в Instagram:",
                                   url="https://instagram.com/mr_sbaziev?igshid=OGIzYTJhMTRmYQ=="),
        types.InlineKeyboardButton("Просмотр товаров:",
                                   url="https://instagram.com/mr_sbaziev?igshid=OGIzYTJhMTRmYQ==")

    )
    kb.add(types.InlineKeyboardButton("О нас:", callback_data='about'))
    await message.reply(f"Привет, {message.from_user.first_name}! "
                        f"Добро пожаловать в магазин аппликация страз.",
                        reply_markup=kb)


# @dp.message_handler(commands=['myinfo'])
async def myinfo(message: types.Message):
    """Информирует пользователю его ID,username,first name"""
    user_id = message.from_user.id
    first_name = message.from_user.first_name
    username = message.from_user.username

    await message.reply(f"Ваш ID: {user_id}\n"
                        f"Ваше имя: {first_name}\n"
                        f"Ваш ник: {username}")


# @dp.message_handler(commands=['picture'])
async def send_picture(message: types.Message):
    """Отправляет рандомные фотки"""
    photos_path = "images"
    photo_files = os.listdir(photos_path)
    random_photo = random.choice(photo_files)
    photos_path = os.path.join(photos_path, random_photo)
    with open(photos_path, 'rb') as photo_file:
        await bot.send_photo(
            chat_id=message.chat.id,
            photo=photo_file.read()
        )
