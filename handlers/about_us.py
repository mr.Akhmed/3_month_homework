from aiogram import types


async def about_us(callback: types.CallbackQuery):
    """Информация о нас"""
    await callback.message.answer(
        "Мы работаем на рынке с 2005 года,"
        "находимся на территории города Бишкек р/к Мадина.")
