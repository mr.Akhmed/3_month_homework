from aiogram import executor, types
from aiogram.dispatcher.filters import Text

from config import dp, scheduler
from db.queries import create_table, drop_tables, init_db, populate_tables
from handlers.about_us import about_us
from handlers.admin_group import catch_bad_words  # pin_message,; ban_group_user
# from handlers.reminder import reminder
from handlers.start import myinfo, send_picture, start
from handlers.tovar import product, show_clippings, show_stones, tovar_all
from handlers.user_form import register_survey_handlers


async def bot_start(_):
    init_db()
    drop_tables()
    create_table()
    populate_tables()


if __name__ == "__main__":
    dp.register_message_handler(start, commands=["start"])
    dp.register_message_handler(myinfo, commands=["myinfo"])
    dp.register_message_handler(send_picture, commands=["picture"])
    dp.register_message_handler(product, commands=["tovar"])
    dp.register_message_handler(show_stones, Text("Стразы"))
    dp.register_message_handler(show_clippings, Text("Вырезка"))
    dp.register_message_handler(tovar_all, Text("Товары и цены"))
    dp.register_callback_query_handler(about_us, Text('about'))

    ''' Команды и функции бота '''

    # dp.register_message_handler(ban_group_user,
    #                             commands="ban",
    #                             commands_prefix="!")
    # dp.register_message_handler(pin_message,
    #                             commands='pin',
    #                             commands_prefix='!')

    '''Планировщик'''

    # dp.register_message_handler(reminder)

    '''Анкета'''
    register_survey_handlers(dp)

    """для выявление непристойных слов"""
    dp.register_message_handler(catch_bad_words)

    scheduler.start()
    executor.start_polling(
        dp,
        on_startup=bot_start,
        skip_updates=True
    )
