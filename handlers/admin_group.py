from aiogram import types

from config import bot


# async def check_author_is_admin(message: types.Message):
#     member_type = await message.chat.get_member(
#         message.from_user.id
#     )
#     pprint(member_type)
#     return member_type["status"] in ["administrator", "creator"]

bad_words = ("дурак", "лом")


async def catch_bad_words(message: types.Message):
    if message.chat.type != "private":
        for word in bad_words:
            if word in message.text.casefold():
                chat_id = message.chat.id
                user_id = message.from_user.id
                admins = await bot.get_chat_administrators(chat_id)
                admin_id = [admin.user.id for admin in admins]
                if user_id in admin_id:
                    await message.reply("Уважаемый админ, тут нельзя так выражаться!")
                    await message.delete()
                else:
                    await message.reply("BAN")
                    await message.delete()
                    await bot.kick_chat_member(message.chat.id,
                                               message.from_user.id
                                               )
                    break

# async def pin_message(message: types.Message):
#     if message.chat.type != "private":
#         is_admin = await check_author_is_admin(message)
#         reply = message.reply_to_message
#         if reply is not None and is_admin:
#             await message.reply_to_message.pin()


# async def ban_group_user(message: types.Message):
#     if message.chat.type != "private":
#         reply = message.reply_to_message
#         is_admin = check_author_is_admin(reply)
#         if reply is not None and is_admin:
#             await message.bot.ban_chat_member(
#                 chat_id=message.chat.id,
#                 user_id=reply.from_user.id
#             )
