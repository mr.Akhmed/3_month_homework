from pprint import pprint

from aiogram import types

from db.queries import get_tovar


async def product(message: types.Message):
    """Информирует что мы принимаем на заказ"""
    kb = types.ReplyKeyboardMarkup()
    kb.add(
        types.KeyboardButton("Стразы"),
        types.KeyboardButton("Вырезка"),
        types.KeyboardButton("Товары и цены")
    )
    await message.answer(
        "Выберите интересующее вам товар:",
        reply_markup=kb
    )


async def tovar_all(message: types.Message):
    """Информирует товары и цены на них"""
    tovars = get_tovar()
    tovar = list(tovars)
    pprint(tovar)
    c = 1
    for i in tovar:
        await message.answer(f'{c}) {i[0]} из {i[1]} цена: {i[2]} сом')
        c += 1


async def show_stones(message: types.Message):
    """Информирует заказ стразы из какого продукта"""
    await message.answer("Из стекло, "
                         "Из пластики, "
                         "Из железы")


async def show_clippings(message: types.Message):
    """Информирует какие материалы мы вырезаем"""
    await message.answer("Вырезка из бархатного материала, "
                         "Вырезка из синтетического материала")
